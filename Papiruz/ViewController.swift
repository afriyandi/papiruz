//
//  ViewController.swift
//  Papiruz
//
//  Created by Afriyandi Setiawan on 5/16/16.
//  Copyright © 2016 pheNyok. All rights reserved.
//

import UIKit
import QuartzCore
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet var qrDecodeLabel: UILabel!
    @IBOutlet var detectorModeSelector: UISegmentedControl!
    
    var videoFilter: CoreImageVideoFilter?
    var detector: CIDetector?
    var countDownVal = 2
    var isCount:Bool = false
    var timer:NSTimer?
    var overlay:CIImage!
    var confidentVal: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        // Create the video filter
        videoFilter = CoreImageVideoFilter(superview: view, applyFilterCallback: nil)
        
        // Simulate a tap on the mode selector to start the process
        detectorModeSelector.selectedSegmentIndex = 0
        handleDetectorSelectionChange(detectorModeSelector)
        confidentVal = 0
    }
    
    @IBAction func handleDetectorSelectionChange(sender: UISegmentedControl) {
        if let videoFilter = videoFilter {
            videoFilter.stopFiltering()
            self.qrDecodeLabel.hidden = true
            
            switch sender.selectedSegmentIndex {
            case 0:
                detector = prepareRectangleDetector()
                videoFilter.applyFilter = {
                    image in
                    return self.performRectangleDetection(image)
                }
            case 1:
                self.qrDecodeLabel.hidden = false
                detector = prepareQRCodeDetector()
                videoFilter.applyFilter = {
                    image in
                    let found = self.performQRCodeDetection(image)
                    dispatch_async(dispatch_get_main_queue()) {
                        if found.decode != "" {
                            self.qrDecodeLabel.text = found.decode
                        }
                    }
                    return found.outImage
                }
            default:
                videoFilter.applyFilter = nil
            }
            
            videoFilter.startFiltering()
        }
    }
    
    @IBAction func unwindToMain(segue: UIStoryboardSegue) {}
    
    //MARK: Utility methods
    func performRectangleDetection(image: CIImage) -> CIImage? {
        var resultImage: CIImage?
        
        if let detector = detector {
            // Get the detections
            let features = detector.featuresInImage(image, options: [CIDetectorAspectRatio: 1.414])
            if features.count == 0 {
                confidentVal = 0
                countDownVal = 2
                isCount = false
                if self.timer != nil {
                    self.timer?.invalidate()
                    self.timer = nil
                }
                guard let dev = videoFilter?.input.device else {
                    return resultImage
                }
                
                do {
                    try dev.lockForConfiguration()
                    dev.focusPointOfInterest = CGPointMake(self.view.frame.width/2, self.view.frame.height/2)
                    dev.focusMode = .ContinuousAutoFocus
                    dev.unlockForConfiguration()
                }
                catch {
                    print("Error")
                }
                
                return resultImage
            }
            var halfPerimiterValue:Float = 0
            var theFeature:CIRectangleFeature? = nil
            for feature in features as! [CIRectangleFeature] {
                let p1:CGPoint = feature.topLeft
                let p2:CGPoint = feature.topRight
                let width:Float = hypotf(Float(p1.x) - Float(p2.x), Float(p1.y) - Float(p2.y))
                let p3:CGPoint = feature.topLeft
                let p4:CGPoint = feature.bottomLeft
                let height:Float = hypotf(Float(p3.x) - Float(p4.x), Float(p3.y) - Float(p4.y))
                let currentHalfPerimeterValue:Float = height + width
                
                if (halfPerimiterValue < currentHalfPerimeterValue) {
                    halfPerimiterValue = currentHalfPerimeterValue
                    theFeature = feature
                }
            }
            if theFeature == nil {
                theFeature = (features as! [CIRectangleFeature]).first!
            }
            resultImage = drawHighlightOverlayForPoints(image, topLeft: theFeature!.topLeft, topRight: theFeature!.topRight,
                                                        bottomLeft: theFeature!.bottomLeft, bottomRight: theFeature!.bottomRight)
        }
        
        return resultImage
    }
    
    func performQRCodeDetection(image: CIImage) -> (outImage: CIImage?, decode: String) {
        var resultImage: CIImage?
        var decode = ""
        if let detector = detector {
            let features = detector.featuresInImage(image)
            for feature in features as! [CIQRCodeFeature] {
                resultImage = drawHighlightOverlayForPoints(image, topLeft: feature.topLeft, topRight: feature.topRight,
                                                            bottomLeft: feature.bottomLeft, bottomRight: feature.bottomRight)
                decode = feature.messageString
            }
        }
        return (resultImage, decode)
    }
    
    func prepareRectangleDetector() -> CIDetector {
        let options: [String: AnyObject] = [CIDetectorAccuracy: CIDetectorAccuracyHigh, CIDetectorMinFeatureSize: 0.6]
        return CIDetector(ofType: CIDetectorTypeRectangle, context: nil, options: options)
    }
    
    func prepareQRCodeDetector() -> CIDetector {
        let options = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        return CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: options)
    }
    
    func drawHighlightOverlayForPoints(image: CIImage, topLeft: CGPoint, topRight: CGPoint,
                                       bottomLeft: CGPoint, bottomRight: CGPoint) -> CIImage {
        confidentVal = confidentVal! + 0.5
        var overlay_ = CIImage(color: CIColor(red: 1.0, green: 0, blue: 0, alpha: 0.5))
        overlay_ = overlay_.imageByCroppingToRect(image.extent)
        overlay_ = overlay_.imageByApplyingFilter("CIPerspectiveTransformWithExtent",
                                                  withInputParameters: [
                                                    "inputExtent": CIVector(CGRect: image.extent),
                                                    "inputTopLeft": CIVector(CGPoint: topLeft),
                                                    "inputTopRight": CIVector(CGPoint: topRight),
                                                    "inputBottomLeft": CIVector(CGPoint: bottomLeft),
                                                    "inputBottomRight": CIVector(CGPoint: bottomRight)
            ])
        //
        //        overlay = image
        //        overlay = overlay.imageByCroppingToRect(image.extent)
        //        overlay = overlay.imageByApplyingFilter("CIPerspectiveCorrection",
        //                                                withInputParameters: [
        //                                                    //"inputExtent": CIVector(CGRect: image.extent),
        //                                                    "inputTopLeft": CIVector(CGPoint: topLeft),
        //                                                    "inputTopRight": CIVector(CGPoint: topRight),
        //                                                    "inputBottomLeft": CIVector(CGPoint: bottomLeft),
        //                                                    "inputBottomRight": CIVector(CGPoint: bottomRight)
        //            ])
        overlay = overlay.imageByCroppingToRect(image.extent)
        overlay = overlay.imageByApplyingFilter("CIPerspectiveCorrection", withInputParameters: [
            "inputTopLeft": CIVector(CGPoint: topLeft),
            "inputTopRight": CIVector(CGPoint: topRight),
            "inputBottomLeft": CIVector(CGPoint: bottomLeft),
            "inputBottomRight": CIVector(CGPoint: bottomRight)
            ]
        )
        startCount()
        return overlay_.imageByCompositingOverImage(image)
    }
    
    func correctPerspective(image:CIImage, feature: CIRectangleFeature) -> CIImage {
        return image.imageByApplyingFilter("CIPerspectiveCorrection", withInputParameters: [
            "inputTopLeft": CIVector(CGPoint: feature.topLeft),
            "inputTopRight": CIVector(CGPoint: feature.topRight),
            "inputBottomLeft": CIVector(CGPoint: feature.bottomLeft),
            "inputBottomRight": CIVector(CGPoint: feature.bottomRight)
            ]
        )
    }
    
    func startCount() {
        if isCount == false {
            dispatch_async(dispatch_get_main_queue(), {
                self.timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(ViewController.countDown), userInfo: nil, repeats: true)
            })
            isCount = true
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let dest = segue.destinationViewController as! ImagePreview
        guard let theImage = overlay else {
            print("gomene")
            return
        }
        dest.theImage = theImage
        videoFilter?.stopFiltering()
        videoFilter?.stilImage = nil
        videoFilter = nil
    }
    
    func countDown(data: NSTimer) {
        if countDownVal < 0 {
            //            var vidCon:AVCaptureConnection? = nil
            guard let stil = videoFilter?.stilImage else {
                return
            }
            
            
            //            for connection:AVCaptureConnection in stil.connections as! [AVCaptureConnection] {
            //                for port:AVCaptureInputPort in connection.inputPorts as! [AVCaptureInputPort] {
            //                    if port.mediaType == AVMediaTypeVideo {
            //                        vidCon = connection
            //                        break
            //                    }
            //                }
            //                if ((vidCon) != nil) {break}
            //            }
            guard let vidCon = stil.connectionWithMediaType(AVMediaTypeVideo) else {
                return
            }
            
            if vidCon.supportsVideoOrientation {
                vidCon.videoOrientation = .Portrait
            }
            
            videoFilter?.stilImage.captureStillImageAsynchronouslyFromConnection(vidCon, completionHandler: { (sampleBuff, error) in
                if error != nil {
                    return
                }
                
                let imageData:NSData? = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuff)
                let imageCI:CIImage = CIImage(data: imageData!, options: [kCIImageColorSpace:NSNull()])!
                self.detector = self.prepareRectangleDetector()
                let rectFeature:[CIRectangleFeature] = self.detector?.featuresInImage(imageCI, options: [CIDetectorAspectRatio: 1.4141, CIDetectorFocalLength: 18]) as! [CIRectangleFeature]
                if rectFeature.count == 0 {
                    self.overlay = imageCI
                    self.performSegueWithIdentifier("gotoPreview", sender: self)
                    return
                }
                
                var halfPerimiterValue:Float = 0
                var theFeature:CIRectangleFeature? = nil
                for feature in rectFeature {
                    let p1:CGPoint = feature.topLeft
                    let p2:CGPoint = feature.topRight
                    let width:Float = hypotf(Float(p1.x) - Float(p2.x), Float(p1.y) - Float(p2.y))
                    let p3:CGPoint = feature.topLeft
                    let p4:CGPoint = feature.bottomLeft
                    let height:Float = hypotf(Float(p3.x) - Float(p4.x), Float(p3.y) - Float(p4.y))
                    let currentHalfPerimeterValue:Float = height + width
                    
                    if (halfPerimiterValue < currentHalfPerimeterValue) {
                        halfPerimiterValue = currentHalfPerimeterValue
                        theFeature = feature
                    }
                }
                if theFeature == nil {
                    theFeature = rectFeature.first!
                }
                
                //                self.overlay = self.correctPerspective(imageCI, feature: theFeature!)
                self.confidentVal = 0
                self.countDownVal = 2
                self.videoFilter?.stopFiltering()
                self.videoFilter = nil
                self.isCount = false
                self.timer?.invalidate()
                self.timer = nil
                self.performSegueWithIdentifier("gotoPreview", sender: self)
                
            })
        } else {
            countDownVal = countDownVal - 1
            print(countDownVal)
        }
        //        print(data.userInfo)
    }
    
}

