//
//  CoreImageVideoFilter.swift
//  Papiruz
//
//  Created by Afriyandi Setiawan on 5/16/16.
//  Copyright © 2016 pheNyok. All rights reserved.
//

import UIKit
import GLKit
import AVFoundation
import CoreMedia
import CoreImage
import OpenGLES
import QuartzCore

class CoreImageVideoFilter: NSObject, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    var applyFilter: ((CIImage) -> CIImage?)?
    var videoDisplayView: GLKView!
    var videoDisplayViewBounds: CGRect!
    var renderContext: CIContext!
    var stilImage:AVCaptureStillImageOutput!
    
    
    var avSession: AVCaptureSession?
    var sessionQueue: dispatch_queue_t!
    
    var input:AVCaptureDeviceInput!
    
    var detector: CIDetector?
    
    init(superview: UIView, applyFilterCallback: ((CIImage) -> CIImage?)?) {
        self.applyFilter = applyFilterCallback
        videoDisplayView = GLKView(frame: superview.bounds, context: EAGLContext(API: .OpenGLES2))
        videoDisplayView.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        videoDisplayView.translatesAutoresizingMaskIntoConstraints = true
        videoDisplayView.contentScaleFactor = 1.0
//        videoDisplayView.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2))
//        videoDisplayView.frame = superview.bounds
        superview.addSubview(videoDisplayView)
        superview.sendSubviewToBack(videoDisplayView)
        
        renderContext = CIContext(EAGLContext: videoDisplayView.context)
        sessionQueue = dispatch_queue_create("AVSessionQueue", DISPATCH_QUEUE_SERIAL)
        
        videoDisplayView.bindDrawable()
        videoDisplayViewBounds = CGRect(x: 0, y: 0, width: videoDisplayView.drawableWidth, height: videoDisplayView.drawableHeight)
    }
    
    deinit {
        stopFiltering()
    }
    
    func startFiltering() {
        // Create a session if we don't already have one
        if avSession == nil {
            avSession = createAVSession()
        }
        
        // And kick it off
        avSession?.startRunning()
    }
    
    func stopFiltering() {
        // Stop the av session
        avSession?.stopRunning()
    }
    
    func createAVSession() -> AVCaptureSession {
        // Input from video camera
        let device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
//
//        do {
//            try device.lockForConfiguration()
//            device.focusMode = AVCaptureFocusMode.ContinuousAutoFocus
//            device.autoFocusRangeRestriction = AVCaptureAutoFocusRangeRestriction.Near
//            device.exposureMode = AVCaptureExposureMode.ContinuousAutoExposure
//            device.unlockForConfiguration()
//        }
//        catch {
//            print("Error")
//        }
//        
        do {
            input = try AVCaptureDeviceInput(device: device)
        } catch let error as NSError {
            input = nil
            let alert = UIAlertController(title: "App Will Close", message: "Sorry for incovenient", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Close", style: .Cancel, handler: { _ in
                print(error.description)
                exit(EXIT_FAILURE)
            }))
            let appDelegate  = UIApplication.sharedApplication().delegate as! AppDelegate
            let viewController = appDelegate.window!.rootViewController as! ViewController
            viewController.presentViewController(alert, animated: true, completion: { 
            })
        }
        
        // Start out with low quality
        let session = AVCaptureSession()
        session.sessionPreset = AVCaptureSessionPresetPhoto
        
        // Output
        let videoOutput = AVCaptureVideoDataOutput()
        stilImage = AVCaptureStillImageOutput()
        
        videoOutput.videoSettings = nil
        videoOutput.alwaysDiscardsLateVideoFrames = true
        videoOutput.setSampleBufferDelegate(self, queue: sessionQueue)
        
        // Join it all together
        session.addInput(input)
        session.addOutput(videoOutput)
        session.addOutput(stilImage)
        let connection = videoOutput.connectionWithMediaType(AVMediaTypeVideo)
        if connection.supportsVideoOrientation {
            print("it is supported")
            connection.videoOrientation = .Portrait
        }
        session.commitConfiguration()
        
        return session
    }
    
    
    
    //MARK: <AVCaptureVideoDataOutputSampleBufferDelegate
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {
        
        // Need to shimmy this through type-hell
        let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        // Force the type change - pass through opaque buffer
        let opaqueBuffer = Unmanaged<CVImageBuffer>.passUnretained(imageBuffer!).toOpaque()
        let pixelBuffer = Unmanaged<CVPixelBuffer>.fromOpaque(opaqueBuffer).takeUnretainedValue()
        
        let sourceImage = CIImage(CVPixelBuffer: pixelBuffer, options: nil)
        
        // Do some detection on the image
        let detectionResult = applyFilter?(sourceImage)
        var outputImage = sourceImage
        if detectionResult != nil {
            outputImage = detectionResult!
        }
        
        // Do some clipping
        var drawFrame = outputImage.extent
        let imageAR = drawFrame.width / drawFrame.height
        let viewAR = videoDisplayViewBounds.width / videoDisplayViewBounds.height
        if imageAR > viewAR {
            drawFrame.origin.x += (drawFrame.width - drawFrame.height * viewAR) / 2.0
            drawFrame.size.width = drawFrame.height / viewAR
        } else {
            drawFrame.origin.y += (drawFrame.height - drawFrame.width / viewAR) / 2.0
            drawFrame.size.height = drawFrame.width / viewAR
        }
        
        videoDisplayView.bindDrawable()
        if videoDisplayView.context != EAGLContext.currentContext() {
            EAGLContext.setCurrentContext(videoDisplayView.context)
        }
        
        // clear eagl view to grey
        glClearColor(0.5, 0.5, 0.5, 1.0);
        glClear(0x00004000)
        
        // set the blend mode to "source over" so that CI will use that
        glEnable(0x0BE2);
        glBlendFunc(1, 0x0303);
        
        renderContext.drawImage(outputImage, inRect: videoDisplayViewBounds, fromRect: drawFrame)
        
        videoDisplayView.display()
        
    }
}