//
//  imagePreview.swift
//  Papiruz
//
//  Created by Afriyandi Setiawan on 5/16/16.
//  Copyright © 2016 pheNyok. All rights reserved.
//

import UIKit

class ImagePreview: UIViewController {

    @IBOutlet weak var image: UIImageView!
    var theImage:CIImage!
    @IBOutlet weak var backButt: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("loaded")
        image.image = UIImage(CIImage: theImage)
        self.view.bringSubviewToFront(backButt)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func backButtAct(sender: AnyObject) {
    }

}
